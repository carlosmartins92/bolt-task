import { createContext } from 'react';
import { DateTime, Task } from '../../app/types/graphql-zeus';

type TaskContext = {
  id?: number;
  subject?: string;
  complete?: boolean;
  complete_date?: DateTime;
  created_at?: DateTime;
  updated_at?: DateTime;
};
export default createContext<Task | TaskContext>({});
