import formatDistance from 'date-fns/formatDistance';
import format from 'date-fns/format';
const today = new Date();

export function formatDate(created_at, prefix = 'Created') {
  return `${prefix} ${format(new Date(created_at), 'yyyy/MM/dd hh:mm')}`;
}

export function formatDateDistance(updated_at, prefix = 'Update') {
  return `${prefix} ${formatDistance(new Date(updated_at), today)}`;
}
