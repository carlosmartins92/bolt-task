import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function ProjectDialog({ open, children, handleClose }) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="project-dialog-title"
    >
      <DialogTitle id="project-dialog-title">Project</DialogTitle>
      <DialogContent>
        <DialogContentText>
          After create the project you are ready to create the tasks ,
        </DialogContentText>
        {children}
      </DialogContent>
    </Dialog>
  );
}
