import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ProfileAvatar from '../../profile/ProfileAvatar';

const useStyles = makeStyles(({ spacing, breakpoints }) => ({
  header: {
    fontWeight: 900,
    minWidth: 0,
    fontSize: 18,
  },
  grow: {
    flexGrow: 1,
  },
  search: {
    position: 'relative',
    marginRight: 8,
    marginLeft: 'auto',
    width: '100%',
    [breakpoints.up('sm')]: {
      marginLeft: spacing(1),
      width: 'auto',
    },
  },
}));

export default function HeaderContent({}) {
  const classes = useStyles();
  return (
    <>
      <Typography noWrap color={'textSecondary'} className={classes.header}>
        BoltTask
      </Typography>
      <div className={classes.grow} />
      <div className={classes.search}>
        <ProfileAvatar />
      </div>
    </>
  );
}
