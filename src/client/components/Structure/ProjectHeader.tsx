import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Item, Row } from '@mui-treasury/components/flex';
import Chip from '@material-ui/core/Chip';
import ProjectContext from '../context/ProjectContext';
import ProjectActionButton from '../Project/ProjectActionButton';

const useStyles = makeStyles(() => ({
  header: {
    backgroundColor: '#fff',
  },
  headline: {
    color: '#122740',
    fontSize: '1.25rem',
    fontWeight: 600,
  },
  link: {
    color: '#2281bb',
    padding: '0 0.25rem',
    fontSize: '0.875rem',
  },
  actions: {
    color: '#BDC9D7',
  },
}));

function filterComplete(tasks, complete = true) {
  return tasks.filter((task) => task.complete === complete);
}

export default function ProjectHeader({ extraActions = null }) {
  const styles = useStyles();
  const { alias, tasks } = useContext(ProjectContext);
  const hasTask = Boolean(tasks && tasks.length);
  return (
    <Row wrap p={2} alignItems={'baseline'} className={styles.header}>
      <Item stretched className={styles.headline}>
        Project: {alias}
      </Item>
      <Item className={styles.actions}>
        <Row gap={1}>
          <Item position={'middle'}>
            <ProjectActionButton label={'update'} size={'small'} />
          </Item>
          <Item position={'middle'}>
            {!hasTask && (
              <Chip size="small" variant={'outlined'} label={`no tasks`} />
            )}
            {hasTask && (
              <Chip
                size="small"
                color={'primary'}
                label={`${filterComplete(tasks, true).length} done`}
              />
            )}
            {hasTask && (
              <Chip
                size="small"
                color={'secondary'}
                label={`${filterComplete(tasks, false).length} to-do`}
              />
            )}
          </Item>
          <Item position={'middle'}>{extraActions}</Item>
        </Row>
      </Item>
    </Row>
  );
}
