import { toast } from 'react-toastify';
import { useTypedMutation } from '../../../app/apollo-client';
import { $ } from '../../../app/types/graphql-zeus';

export function useTaskUpdate({ id, onChange, setError }) {
  const [updateSubject] = useTypedMutation(
    {
      updateSubject: [
        {
          subject: $`subject`,
          id: id,
        },
        { subject: true, complete: true },
      ],
    },
    {
      onCompleted: () => {
        onChange();
      },
      onError: (e) => {
        setError('subject', {
          type: 'manual',
          message: e.message,
        });
      },
    },
  );
  return (subject) =>
    updateSubject({
      variables: {
        subject,
      },
    });
}

export function useTaskCreate({ id, onChange, setError }) {
  const [createTask] = useTypedMutation(
    {
      createTask: [
        {
          subject: $`subject`,
          projectId: id,
          complete: false,
        },
        { subject: true, complete: true },
      ],
    },
    {
      onCompleted: () => {
        onChange();
      },
      onError: (e) => {
        setError('subject', {
          type: 'manual',
          message: e.message,
        });
      },
    },
  );
  return (subject) =>
    createTask({
      variables: {
        subject: subject,
      },
    });
}

export function useToggleTask({ id, onChange }) {
  const [toggleTask] = useTypedMutation(
    {
      toggleTask: [
        {
          id: id,
        },
        { complete: true },
      ],
    },
    {
      onCompleted: () => {
        onChange();
      },
      onError: (e) => {
        toast.error(e.message);
      },
    },
  );
  return toggleTask;
}
