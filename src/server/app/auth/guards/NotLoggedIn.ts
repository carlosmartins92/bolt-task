import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

@Injectable()
export class NotLoggedIn implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const response = context.switchToHttp().getResponse();
    return this.validateRequest(request, response);
  }

  async validateRequest(request, response) {
    const tokenExists = request.cookies.jwt;
    if (tokenExists) {
      response.redirect('/projects');
    }
    return true;
  }
}
