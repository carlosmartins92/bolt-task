import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request, Response } from 'express';

import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { DoesUserExist } from './guards/DoesUserExist';
import { JwtAuthGuard } from './jwt/jwt-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Req() req: Request, @Res() res: Response) {
    const newVar = await this.authService.login(req.user);
    if (newVar && newVar.token) {
      res.cookie('jwt', newVar.token);
      return res.status(201).json(newVar);
    }
    return res.status(401).json(newVar);
  }

  @UseGuards(DoesUserExist)
  @Post('signup')
  async signUp(@Body() user: CreateUserDto, @Res() res: Response) {
    const responseData = await this.authService.create(user);
    if (responseData && responseData.token) {
      res.cookie('jwt', responseData.token);
      return res.status(201).json(responseData);
    }
    return res.status(401).json(responseData);
  }

  @UseGuards(JwtAuthGuard)
  @Get('logout')
  logout(@Res() res) {
    res.clearCookie('jwt');
    return res.redirect('/');
  }
}
