import { Inject, UseGuards } from '@nestjs/common';
import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { Like } from 'typeorm';

import { CurrentUser } from '../auth/graphql/gql-auth.decorator';
import { GqlAuthGuard } from '../auth/graphql/gql-auth.guard';
import { User } from '../users/user.entity';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';

@Resolver((_of) => Task)
export class TasksResolver {
  constructor(@Inject(TasksService) private tasksService: TasksService) {}

  @Query((_returns) => [Task])
  @UseGuards(GqlAuthGuard)
  tasks(@CurrentUser() user: User) {
    return this.tasksService.findAll({ where: { user: user } });
  }

  @ResolveField()
  project(@Parent() task: Task) {
    return this.tasksService.findOne({
      where: { id: task.project.id },
    });
  }

  @Query((_returns) => [Task])
  @UseGuards(GqlAuthGuard)
  searchTasksByProject(
    @CurrentUser() user: User,
    @Args({ name: 'projectId', type: () => Number }) projectId: number,
    @Args({ name: 'subject', nullable: true, type: () => String })
    subject: string,
  ) {
    return this.tasksService.findAll({
      where: {
        user: user,
        project: { id: projectId },
        subject: Like(`${subject}%`),
      },
    });
  }

  @Mutation((_returns) => Task)
  @UseGuards(GqlAuthGuard)
  createTask(
    @CurrentUser() user: User,
    @Args({ name: 'projectId', type: () => Number }) projectId: number,
    @Args({ name: 'subject', type: () => String }) subject: string,
    @Args({
      name: 'complete',
      type: () => Boolean,
      defaultValue: false,
      nullable: true,
    })
    complete: boolean,
  ) {
    return this.tasksService.createFromProjectDetails({
      projectId: projectId,
      user: user,
      subject: subject,
      complete: complete,
    });
  }


  @Mutation((_returns) => Task)
  @UseGuards(GqlAuthGuard)
  toggleTask(@Args({ name: 'id', type: () => Number }) id: number) {
    return this.tasksService.toggleTask(id);
  }

  @Mutation((_returns) => Task)
  @UseGuards(GqlAuthGuard)
  updateSubject(
    @Args({ name: 'id', type: () => Number }) id: number,
    @Args({ name: 'subject', type: () => String }) subject: string,
  ) {
    return this.tasksService.updateSubject(id, subject);
  }

  @Mutation((_returns) => Task)
  @UseGuards(GqlAuthGuard)
  removeTask(@Args({ name: 'id', type: () => Number }) id: number) {
    return this.tasksService.remove(id);
  }
}
