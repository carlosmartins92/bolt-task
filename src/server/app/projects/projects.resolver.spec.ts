import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnection } from 'typeorm';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { ProjectsModule } from './projects.module';
import { ProjectsResolver } from './projects.resolver';
import { ProjectsService } from './projects.service';
import { projectsFactory, tasksFactory, usersFactory } from 'test/factories';

describe('ProjectsResolver', () => {
  let resolver: ProjectsResolver;
  let projectsService: ProjectsService;
  let usersService: UsersService;
  let moduleRef: TestingModule;

  beforeEach(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          url: process.env.DATABASE_URL,
        }),
        ProjectsModule,
        UsersModule,
      ],
    }).compile();

    resolver = moduleRef.get<ProjectsResolver>(ProjectsResolver);
    projectsService = moduleRef.get<ProjectsService>(ProjectsService);
    usersService = moduleRef.get<UsersService>(UsersService);

    await getConnection().synchronize(true);
  });

  afterEach(async () => {
    await moduleRef.close();
  });

  describe('projects', () => {
    it('returns projects of user', async () => {
      const user = await usersService.create(usersFactory.build());
      const project = await projectsService.create(
        projectsFactory.build({}, { associations: { user: user } }),
      );
      const result = await resolver.projects(user);
      expect(project.alias).toMatch(result[0].alias);
    });
    it('does not return projects of other user', async () => {
      const anotherUser = await usersService.create(usersFactory.build());
      await projectsService.create(
        projectsFactory.build({}, { associations: { user: anotherUser } }),
      );
      const user = await usersService.create(usersFactory.build());
      const result = await resolver.projects(user);
      expect(result).toEqual([]);
    });
  });

  describe('createProject', () => {
    it('returns the project', async () => {
      const user = await usersService.create(usersFactory.build());
      const alias = projectsFactory.build().alias;
      const result = await resolver.createProject(user, alias);
      expect(result).toMatchObject({ alias: alias });
    });

    it('projectByAlias search like% and user', async () => {
      const user = await usersService.create(usersFactory.build());
      const anotherUser = await usersService.create(usersFactory.build());
      await resolver.createProject(user, 'hello Bolt');
      let result = await resolver.projectByAlias(user, 'hello');
      expect(result.length).toEqual(1);
      result = await resolver.projectByAlias(anotherUser, 'hello');
      expect(result.length).toEqual(0);
      result = await resolver.projectByAlias(user, 'Bol');
      expect(result.length).toEqual(0);
    });

    it('projectById search like% and user', async () => {
      const user = await usersService.create(usersFactory.build());
      const anotherUser = await usersService.create(usersFactory.build());
      const project = await resolver.createProject(user, 'hello Bolt');
      let result = await resolver.projectById(user, project.id);
      expect(result.alias).toEqual(project.alias);
      result = await resolver.projectById(anotherUser, project.id);
      expect(result).toBeUndefined();
    });

    it('creates an project', async () => {
      const user = await usersService.create(usersFactory.build());
      const alias = projectsFactory.build().alias;
      await resolver.createProject(user, alias);
      const orderCount = (
        await projectsService.findAll({ where: { user: user } })
      ).length;
      expect(orderCount).toEqual(1);
    });

    it('updateAlias', async () => {
      const user = await usersService.create(usersFactory.build());
      const alias = projectsFactory.build().alias;
      let project = await resolver.createProject(user, alias);
      expect(project.alias).toEqual(alias);
      const newAlias = projectsFactory.build().alias;
      const projectUpdate = await resolver.updateAlias(project.id, newAlias);
      expect(projectUpdate.alias).toBe(newAlias);
    });

    it('removeProject', async () => {
      const user = await usersService.create(usersFactory.build());
      const alias = projectsFactory.build().alias;
      let project = await resolver.createProject(user, alias);
      expect(project.alias).toEqual(alias);
      const removed = await resolver.removeProject(project.id);
      expect(removed.alias).toBe(project.alias);
      const findProject = await projectsService.findOne({ where: { id: project.id } });
      expect(findProject).toBeUndefined();
    });


    it('does not create the same project twice', async () => {
      const user = await usersService.create(usersFactory.build());
      const alias = projectsFactory.build().alias;
      await resolver.createProject(user, alias);
      await resolver.createProject(user, alias);
      const projectCount = (
        await projectsService.findAll({ where: { user: user } })
      ).length;
      expect(projectCount).toEqual(1);
    });
  });
});
