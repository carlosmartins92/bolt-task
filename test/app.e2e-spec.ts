import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { AppModule } from 'src/server/app/app.module';
import { INestApplication } from '@nestjs/common';
import { getConnection } from 'typeorm';
import { projectsFactory, usersFactory } from 'test/factories';
import { UsersService } from 'src/server/app/users/users.service';
import { User } from 'src/server/app/users/user.entity';
import { ProjectsService } from 'src/server/app/projects/projects.service';
import { JwtAuthService } from 'src/server/app/auth/jwt/jwt-auth.service';
import { login } from './utils';
import { Project } from '../src/server/app/projects/project.entity';

describe('Application', () => {
  let app: INestApplication;
  let authService: JwtAuthService;
  let usersService: UsersService;
  let projectsService: ProjectsService;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();

    usersService = app.get(UsersService);
    projectsService = app.get(ProjectsService);
    authService = app.get(JwtAuthService);
  });

  beforeEach(async () => {
    await getConnection().synchronize(true);
  });

  describe('GraphQL', () => {
    const endpoint = '/graphql';
    let agent: request.Test;

    beforeEach(async () => {
      agent = request(app.getHttpServer()).post(endpoint);
    });

    describe('users', () => {
      const query = '{ users { id username } }';
      let user: User;

      beforeEach(async () => {
        user = await usersService.create(usersFactory.build());
      });

      it('returns users', () => {
        return agent
          .send({ query: query })
          .expect(200)
          .expect((res) => {
            expect(res.body.data.users).toHaveLength(1);
            expect(res.body.data.users[0].id).toEqual(user.id);
            expect(res.body.data.users[0].username).toEqual(user.username);
          });
      });
    });

    describe('projects', () => {
      const query = '{ projects { id alias } }';
      let user: User;
      let project: Project;

      it('returns unauthorized', () => {
        return agent
          .send({ query: query })
          .expect(200)
          .expect((res) => {
            expect(res.body.errors).toHaveLength(1);
            expect(res.body.errors[0].message).toEqual('Unauthorized');
          });
      });

      describe('when authorized', () => {
        beforeEach(async () => {
          user = await usersService.create(usersFactory.build());
          project = await projectsService.create(
            projectsFactory.build({}, { associations: { user: user } }),
          );
        });

        it('returns projects', () => {
          return login(agent, user, authService)
            .send({ query: query })
            .expect(200)
            .expect((res) => {
              expect(res.body.data.projects).toHaveLength(1);
              expect(res.body.data.projects[0].id).toEqual(project.id);
            });
        });
      });
    });
  });
});
