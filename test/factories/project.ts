import { Factory } from 'fishery';
import * as faker from 'faker';
import { CreateProjectDto } from 'src/server/app/projects/dto/create-project.dto';

export default Factory.define<CreateProjectDto>(({ associations }) => ({
  alias: faker.lorem.words(),
  user: associations.user,
}));
